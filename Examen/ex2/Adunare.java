package paducel.ioana.ex2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.*;
import java.util.*;

public class Adunare extends JFrame{


    JLabel op1,op2;
    JTextField top1,top2,tRez;
    JButton bAduna;

    Adunare(){

        setTitle("Adunare");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        op1 = new JLabel("Operandul 1 ");
        op1.setBounds(10, 50, width, height);

        op2 = new JLabel("Operandul 2 ");
        op2.setBounds(10, 100,width, height);

        top1 = new JTextField();
        top1.setBounds(80,50,width, height);

        top2 = new JTextField();
        top2.setBounds(80,100,width, height);


        bAduna = new JButton("Aduna");
        bAduna.setBounds(70,150,100, height);

        tRez=new JTextField();
        tRez.setBounds(70,200,width,height);
        tRez.setEditable(false);


        bAduna.addActionListener(new TratareButonAdunare());

        add(op1);add(op2);add(top1);add(bAduna);
        add(top2);add(tRez);

    }

    public static void main(String[] args) {
        new Adunare();
    }

    class TratareButonAdunare implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            String op1 = Adunare.this.top1.getText();
            String op2 =Adunare.this.top2.getText();
            int l1=op1.length();
            int l2=op2.length();
            int  rez=l1+l2;
            tRez.setText(Integer.toString(rez));
        }
    }
}
